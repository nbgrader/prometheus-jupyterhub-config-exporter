#!/usr/bin/env python3

from prometheus_client import start_http_server, Gauge
from prometheus_client import REGISTRY, PROCESS_COLLECTOR, PLATFORM_COLLECTOR, GC_COLLECTOR
from prometheus_client.core import GaugeMetricFamily
import time
import re
import subprocess

##### Configuration (start)

PORTNUMBER = 10019
PATHTOJUPYTERHUBCONFIGFILE = "/var/lib/jupyterhub/venvs/py3/etc/jupyter/jupyterhub_config.py"

##### Configuration (end)

# Removing useless default metrics about python's runtime and garbage collector
REGISTRY.unregister(PROCESS_COLLECTOR)
REGISTRY.unregister(PLATFORM_COLLECTOR)
REGISTRY.unregister(GC_COLLECTOR)


cpuLimitRegex = re.compile("c.SystemdSpawner.cpu_limit\s*=\s*(.*)")
ramLimitRegex = re.compile("c.SystemdSpawner.mem_limit\s*=\s*['|\"](.*)['|\"]")
def collectJHubLimits():
    metrics = []
    with open(PATHTOJUPYTERHUBCONFIGFILE, 'r') as jhubConfigFile:
        config = jhubConfigFile.read()
        # CPU Limit
        matches = re.findall(cpuLimitRegex, config)
        if len(matches) >= 1:
            # Only return the metric if the parsing succeeded
            metrics.append(GaugeMetricFamily('jupyterhub_user_cpu_quota', 'Maximum number of CPUs a single user can use at the same time', value=int(matches[0])))
        # RAM Limit
        matches = re.findall(ramLimitRegex, config)
        if len(matches) >= 1:
            # Only return the metric if the parsing succeeded
            ramValueString = matches[0]
            unitSuffixes = {'K':pow(10,3), 'M':pow(10,6), 'G':pow(10,9)}
            ramValueBytes = float(ramValueString[0:-1]) * unitSuffixes[ramValueString[-1]]
            metrics.append(GaugeMetricFamily('jupyterhub_user_ram_quota', 'Maximum RAM a single user can consume', value=ramValueBytes))
    return metrics

class JupyterhubConfigCollector():
    def collect(self):
        metrics = collectJHubLimits()
        for m in metrics:
            yield m

if __name__ == '__main__':
    print("starting server on port {}".format(PORTNUMBER))
    # Start up the server to expose the metrics.
    start_http_server(PORTNUMBER)
    REGISTRY.register(JupyterhubConfigCollector())
    while True:
        time.sleep(1)


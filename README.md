# Prometheus JupyterHub Config Exporter

This prometheus exporter exports information about jupyterhub that the official jupyterhub prometheus exporter does not expose
It reads the jupyterhub_config.py file to extract the following information:
- CPU limit imposed on each user
- RAM limit imposed on each user

## Settings
(edit at the top of prometheus-jupyterhub-config-exporter.py)
- Uses port 10019 by default
- Set the path to your jupyterhub_config.py file (by default: `/var/lib/jupyterhub/venvs/py3/etc/jupyter/jupyterhub_config.py`)

## Installation (Debian)

- clone this repository to `/usr/local/lib`
- change the group ownership of the `prometheus-jupyterhub-config-exporter.py` file in it to prometheus, by running: `sudo chown root:prometheus /usr/local/lib/prometheus-jupyterhub-config-exporter/prometheus-jupyterhub-config-exporter.py`
- give execution permision on the `prometheus-jupyterhub-config-exporter.py` file, by running: `sudo chmod u=rx,g=rx,o=r /usr/local/lib/prometheus-jupyterhub-config-exporter/prometheus-jupyterhub-config-exporter.py`
- configure systemd to run this exporter and keep it running:
	- copy the `prometheus-jupyterhub-config-exporter.service` file from this repository to `/lib/systemd/system/` by running: `sudo cp /usr/local/lib/prometheus-jupyterhub-config-exporter/prometheus-jupyterhub-config-exporter.service /lib/systemd/system/`
	- enable the service to ensure the service starts when the system boots: `sudo systemctl enable prometheus-jupyterhub-config-exporter.service`
	- start the service with `sudo systemctl start prometheus-jupyterhub-config-exporter.service`
	- check that the service is running, with `sudo systemctl status prometheus-jupyterhub-config-exporter.service`
- you can check that you are indeed getting metrics exported by using `curl http://0.0.0.0:10019/metrics`, which should look like this:
```
# HELP jupyterhub_user_cpu_quota Maximum number of CPUs a single user can use at the same time
# TYPE jupyterhub_user_cpu_quota gauge
jupyterhub_user_cpu_quota 2.0
# HELP jupyterhub_user_ram_quota Maximum RAM a single user can consume
# TYPE jupyterhub_user_ram_quota gauge
jupyterhub_user_ram_quota 3e+09
```

## Notes

If the machine running this exporter has a firewall, make sure your prometheus scraper is allowed to access it on the port the exporter is running on. If the firewall is nftable, you can edit the `/etc/firewall/fragments.d/filter/input.nft` file, and add a line like this to the `chain input` function:
```
ip saddr IPOFYOURPROMETHEUSSCRAPER tcp dport 10019 ct state new counter accept
```
Then restart nftable with:
```
sudo systemctl restart nftables.service
```